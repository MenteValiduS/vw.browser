﻿using System.Windows;
using VW.Browser.Views;

namespace VW.Browser
{
    /// <summary>
    /// Логика взаимодействия для App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void Application_Startup(object sender, StartupEventArgs e)
        {

            if (e.Args.Length > 0)
            {
                DAO.DB_Communication.DBPathString = e.Args[0].ToString(); ;
                MainArea window = new MainArea();
                window.Show();
            }

        }
    }
}
