﻿using VW.Browser.Models;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Text;


namespace VW.Browser.DAO
{
    class DB_Communication
    {
        private static string m_DBPathString;

        public static string DBPathString { get => m_DBPathString; set => m_DBPathString = value; }

        private static string GetConnectionString()
        {

            if (File.Exists(DBPathString))
            {
                return "URI=file:" + DBPathString;
            }

            return string.Empty;
        }

        public static Session[] SelectSessions()
        {
            List<Session> sessionListBuf = new List<Session>();

            SQLiteConnection connection = new SQLiteConnection(GetConnectionString());
            connection.Open();

            using (var cmd = connection.CreateCommand())
            {
                cmd.CommandText = new StringBuilder("select Ga.name, Ge.name, S.play_time, S.[date], S.additional, Ga.image_path " +
                    "from Sessions S inner join Games Ga " +
                    "on S.game = Ga.ID inner join Genres Ge " +
                    "on Ga.genre = Ge.ID").ToString();

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        Session sessionBuf = new Session();
                        sessionBuf.game = reader.GetString(0);
                        sessionBuf.genre = reader.GetString(1);
                        sessionBuf.gameTime = reader.GetInt32(2);
                        sessionBuf.date = reader.GetString(3);
                        sessionBuf.groupDate = GetGroupDate(sessionBuf.date);
                        sessionBuf.additionalInfo = reader.GetString(4);
                        sessionBuf.imagePath = reader.GetString(5);
                        sessionListBuf.Add(sessionBuf);
                    }
                }

            }

            connection.Close();
            return sessionListBuf.ToArray();

        }

        private static string GetGroupDate(string date)
        {
            return DateTime.Parse(date).ToString("MMMM yyyy");
        }
    }
}
