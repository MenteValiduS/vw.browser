﻿using System;
using System.Windows.Input;

namespace VW.Browser.Commands
{
    public class RelayCommand : ICommand
    {
        #region Private Members

        /// <summary>
        /// Объект Action, который будет исполняться.
        /// </summary>
        private Action mAction;

        #endregion

        #region Public Events

        /// <summary>
        /// Данное событие срабатывает, когда значение <see cref="CanExecute(object)"/> изменяется.
        /// </summary>
        public event EventHandler CanExecuteChanged = (sender, e) => { };

        #endregion

        #region Constructor

        /// <summary>
        /// Стандартный конструктор.
        /// </summary>
        public RelayCommand(Action action)
        {
            mAction = action;
        }

        #endregion

        #region Command Methods

        /// <summary>
        /// Определение, сожет ли выполниться команда.
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public bool CanExecute(object parameter)
        {
            return true;
        }

        /// <summary>
        /// Исполняет Action команды.
        /// </summary>
        /// <param name="parameter"></param>
        public void Execute(object parameter)
        {
            mAction();
        }

        #endregion
    }
}