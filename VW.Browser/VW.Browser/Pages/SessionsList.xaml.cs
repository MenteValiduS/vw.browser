﻿using System.Windows.Controls;
using VW.Browser.ViewModels;

namespace VW.Browser.Pages
{
    /// <summary>
    /// Логика взаимодействия для SessionsList.xaml
    /// </summary>
    public partial class SessionsList : Page
    {
        public SessionsList()
        {
            InitializeComponent();

            DataContext = new SessionsListViewModel();
        }
    }
}
