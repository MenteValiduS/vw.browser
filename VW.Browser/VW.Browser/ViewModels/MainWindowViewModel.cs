﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Input;
using VW.Browser.Commands;
using VW.Browser.DAO;
using VW.Browser.Models;
using VW.Browser.Pages;
using _Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;
using System.Windows;
using System.IO;

namespace VW.Browser.ViewModels
{
    class MainWindowViewModel : BaseViewModel
    {
        private Page _currentPage;

        public Page CurrentPage
        {
            get => _currentPage;
            set
            {
                _currentPage = value;
                OnPropertyChanged("CurrentPage");
            }
        }

        public string DB
        {
            get
            {
                return DB_Communication.DBPathString;
            }
            set
            {

            }
        }


        #region Public Commands

        public ICommand GoToStatistics { get; set; }
        public ICommand GoToSessionList { get; set; }
        public ICommand OutputToExcel { get; set; }

        #endregion

        /// <summary>
        /// Стандартный конструктор.
        /// </summary>
        public MainWindowViewModel()
        {
            GoToSessionList = new RelayCommand(SetToSessionsList);
            GoToStatistics = new RelayCommand(SetToStatistics);
            OutputToExcel = new RelayCommand(CreateExcel);
            SetToSessionsList();
        }

        #region Private Command Methods

        private void SetToStatistics()
        {
            CurrentPage = new Statisctics();
        }

        private void SetToSessionsList()
        {
            CurrentPage = new SessionsList();
        }

        private void CreateExcel()
        {
            _Excel.Application oXL;
            _Excel._Workbook oWB;
            _Excel._Worksheet oSheet;
            _Excel.Range oRng;

            try
            {
                //Start Excel and get Application object.
                oXL = new _Excel.Application();
                oXL.Visible = true;

                //Get a new workbook.
                oWB = (_Excel._Workbook)(oXL.Workbooks.Add(Missing.Value));
                oSheet = (_Excel._Worksheet)oWB.ActiveSheet;

                //Add table headers going cell by cell.
                oSheet.Cells[1, 1] = "Название игры";
                oSheet.Cells[1, 2] = "Время";
                oSheet.Cells[1, 3] = "Дата";

                //Format A1:D1 as bold, vertical alignment = center.
                oSheet.get_Range("A1", "D1").Font.Bold = true;
                oSheet.get_Range("A1", "D1").VerticalAlignment =
                _Excel.XlVAlign.xlVAlignCenter;

                List<Session> sessions = Session.GetSessions().OrderBy(s => DateTime.Parse(s.date)).ToList();
                string[,] stats = new string[sessions.Count, 3];

                for (int i = 0; i < sessions.Count; i++)
                {
                    stats[i, 0] = sessions[i].game;
                    stats[i, 1] = (sessions[i].gameTime / 60).ToString();
                    stats[i, 2] = sessions[i].date;
                }

                oSheet.get_Range("A2", "C" + (1 + sessions.Count).ToString()).Value2 = stats;
                

                //AutoFit columns A:D.
                oRng = oSheet.get_Range("A1", "D1");
                oRng.EntireColumn.AutoFit();


                //Make sure Excel is visible and give the user control
                //of Microsoft Excel's lifetime.
                string pathToExcel = Path.Combine(Environment.CurrentDirectory, "Статистика.xlsx");

                oWB.SaveAs(pathToExcel);
                oXL.Visible = true;
                oXL.UserControl = true;
            }
            catch (Exception theException)
            {
                String errorMessage;
                errorMessage = "Error: ";
                errorMessage = String.Concat(errorMessage, theException.Message);
                errorMessage = String.Concat(errorMessage, " Line: ");
                errorMessage = String.Concat(errorMessage, theException.Source);

                MessageBox.Show(errorMessage, "Error");
            }

            System.Diagnostics.Process.Start("explorer.exe", Environment.CurrentDirectory);
        }

        #endregion

    }
}
