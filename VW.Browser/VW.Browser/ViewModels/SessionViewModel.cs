﻿using System.ComponentModel;
using VW.Browser.Models;

namespace VW.Browser.ViewModels
{
    public class SessionViewModel : INotifyPropertyChanged
    {

        #region Private Members

        private Session mSession;



        #endregion

        #region Events

        public event PropertyChangedEventHandler PropertyChanged = (sender, e) => { };

        #endregion

        #region Properties

        public Session Session { get => mSession; set => mSession = value; }

        public string Game
        {
            get => Session.game;

            set
            {
                Session.game = value;
                PropertyChanged(this, new PropertyChangedEventArgs(nameof(Session.game)));
            }
        }

        public string Genre
        {
            get => Session.genre;

            set
            {
                Session.genre = value;
                PropertyChanged(this, new PropertyChangedEventArgs(nameof(Session.genre)));
            }
        }
        public int GameTime
        {
            get => Session.gameTime;
            set
            {
                Session.gameTime = value;
                PropertyChanged(this, new PropertyChangedEventArgs(nameof(Session.gameTime)));

            }
        }
        public string Date
        {
            get => Session.date;
            set
            {
                Session.date = value;
                PropertyChanged(this, new PropertyChangedEventArgs(nameof(Session.date)));

            }
        }
        public string GroupDate
        {
            get => Session.groupDate;
            set
            {
                Session.groupDate = value;
                PropertyChanged(this, new PropertyChangedEventArgs(nameof(Session.groupDate)));

            }
        }
        public string AdditionalInfo
        {
            get => Session.additionalInfo;
            set
            {
                Session.additionalInfo = value;
                PropertyChanged(this, new PropertyChangedEventArgs(nameof(Session.additionalInfo)));
            }
        }
        public string ImagePath
        {
            get => Session.imagePath;
            set
            {
                Session.imagePath = value;
                PropertyChanged(this, new PropertyChangedEventArgs(nameof(Session.imagePath)));
            }
        }


        #endregion

        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        public SessionViewModel(Session session)
        {
            this.Session = session;
        }

        #endregion

    }
}
