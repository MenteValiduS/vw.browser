﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using VW.Browser.Models;

namespace VW.Browser.ViewModels
{
    public class SessionsListViewModel : INotifyPropertyChanged
    {
        #region Private Fields
        /// <summary>
        /// Фильтр по имени игры.
        /// </summary>
        private string filterGameNameText;
        private CollectionViewSource _sessionsCollection;

        #endregion

        #region Events
        /// <summary>
        /// Событие для сообщения модели об изменениях.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        #region Properties
        /// <summary>
        /// Прослушиваемая коллекция всех сессий.
        /// </summary>
        //public ObservableCollection<SessionViewModel> Sessions { get; set; }

        public ICollectionView SessionsCollection
        {
            get
            {
                ObservableCollection<SessionViewModel> sessionViewModels = _sessionsCollection.View as ObservableCollection<SessionViewModel>;
                return CollectionViewSource.GetDefaultView(Collection);
            }
        }

        public string FilterGameNameText
        {
            get
            {
                return filterGameNameText;
            }
            set
            {
                filterGameNameText = value;
                _sessionsCollection.View.Refresh();
                OnPropertyChanged();
            }
        }

        public ObservableCollection<SessionViewModel> Collection { get; set; }

        #endregion

        #region Constructor
        /// <summary>
        /// Стнадартный конструктор.
        /// </summary>
        public SessionsListViewModel()
        {
            _sessionsCollection = new CollectionViewSource();
            Collection = new ObservableCollection<SessionViewModel>(Session.GetSessions().OrderBy(s => DateTime.Parse(s.date)).Select(s => new SessionViewModel(s)));
            _sessionsCollection.Source = Collection;
            _sessionsCollection.Filter += sessionsCollection_Filter;
        }

        private void sessionsCollection_Filter(object sender, FilterEventArgs e)
        {
            if (string.IsNullOrEmpty(FilterGameNameText))
            {
                e.Accepted = true;
                return;
            }

            SessionViewModel session = e.Item as SessionViewModel;

            if (session == null)
                return;

            if (session.Game.ToUpper().Contains(FilterGameNameText.ToUpper()))
            {
                e.Accepted = true;
            }
            else
            {
                e.Accepted = false;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Метод сообщения прелставлению об изменениях.
        /// </summary>
        /// <param name="prop"> Имя вызвавшего свойтсва. </param>
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
        #endregion
    }
}
