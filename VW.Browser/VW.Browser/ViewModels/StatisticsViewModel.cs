﻿using LiveCharts;
using LiveCharts.Wpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Media;
using VW.Browser.Models;

namespace VW.Browser.ViewModels
{
    public class StatisticsViewModel : BaseViewModel
    {

        #region Public Properties
        public SeriesCollection SeriesCollection { get; set; }
        public string[] Labels { get; set; }
        public Func<double, string> YFormatter { get; set; }
        #endregion
        public StatisticsViewModel()
        {
            SeriesCollection = new SeriesCollection();
            //{
            //    new LineSeries
            //    {
            //        Title = "Series 1",
            //        Values = new ChartValues<double> { 4, 6, 5, 2 ,4, 3 }
            //    },
            //    new LineSeries
            //    {
            //        Title = "Series 2",
            //        Values = new ChartValues<double> { 6, 7, 3, 4 ,6, 3 },
            //        PointGeometry = null
            //    },
            //    new LineSeries
            //    {
            //        Title = "Series 3",
            //        Values = new ChartValues<double> { 4,2,7,2,7, 5 },
            //        PointGeometry = DefaultGeometries.Square,
            //        PointGeometrySize = 15
            //    }
            //};


            Labels = new[] { "Янв", "Фев", "Мар", "Апр", "Май", "Июнь", "Июль", "Авг", "Сен", "Окт", "Ноя", "Дек" };
            //YFormatter = value => value.ToString("C");
            ProcessStatistics();

            ////modifying the series collection will animate and update the chart
            //SeriesCollection.Add(new LineSeries
            //{
            //    Title = "Series 4",
            //    Values = new ChartValues<double> { 5, 3, 2, 4 },
            //    LineSmoothness = 0, //0: straight lines, 1: really smooth lines
            //    PointGeometry = Geometry.Parse("m 25 70.36218 20 -28 -20 22 -8 -6 z"),
            //    PointGeometrySize = 50,
            //    PointForeground = Brushes.Gray
            //});

            ////modifying any series values will also animate and update the chart
            //SeriesCollection[3].Values.Add(5d);
        }

        #region Private Methods

        private void ProcessStatistics()
        {
            List<YearStatistic> yearStatistics = GetStatistics();
            foreach (var year in yearStatistics)
            {
                SeriesCollection.Add(new LineSeries
                {
                    Title = year.year.ToString(),
                    Values = new ChartValues<int>(year.playInMonths)
                });

            }
        }

        private List<YearStatistic> GetStatistics()
        {
            List<Session> sessions = new List<Session>(Session.GetSessions());
            List<YearStatistic> yearStatistics = new List<YearStatistic>();

            foreach (Session session in sessions)
            {

                DateTime curDate = DateTime.Parse(session.date);

                if (0 == yearStatistics.Count || !yearStatistics.Exists(y => y.year == curDate.Year))
                {
                    YearStatistic year = new YearStatistic
                    {
                        year = curDate.Year
                    };

                    year.playInMonths[curDate.Month - 1]++;
                    yearStatistics.Add(year);
                }
                else
                {
                    YearStatistic year = yearStatistics.FirstOrDefault(y => y.year == curDate.Year);
                    year.playInMonths[curDate.Month - 1]++;
                }

            }

            return yearStatistics;
        }

        #endregion

    }
}
