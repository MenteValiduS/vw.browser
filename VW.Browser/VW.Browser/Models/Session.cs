﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using VW.Browser.DAO;

namespace VW.Browser.Models
{
    public class Session
    {
        public string game;
        public string genre;
        public int gameTime;
        public string date;
        public string groupDate;
        public string additionalInfo;
        public string imagePath;

        //public string Game
        //{
        //    get => _game;

        //    set
        //    {
        //        _game = value;
        //        OnPropertyChanged();
        //    }
        //}

        //public string Genre
        //{
        //    get => _genre;

        //    set
        //    {
        //        _genre = value;
        //        OnPropertyChanged("Genre");
        //    }
        //}
        //public int GameTime
        //{
        //    get => _gameTime;
        //    set
        //    {
        //        _gameTime = value;
        //        OnPropertyChanged("GameTime");
        //    }
        //}
        //public string Date
        //{
        //    get => _date;
        //    set
        //    {
        //        _date = value;
        //        OnPropertyChanged("Date");
        //    }
        //}
        //public string GroupDate
        //{
        //    get => _groupDate;
        //    set
        //    {
        //        _groupDate = value;
        //        OnPropertyChanged("GroupDate");
        //    }
        //}
        //public string AdditionalInfo
        //{
        //    get => _additionalInfo;
        //    set
        //    {
        //        _additionalInfo = value;
        //        OnPropertyChanged("AdditionalInfo");
        //    }
        //}
        //public string ImagePath
        //{
        //    get => _imagePath;
        //    set
        //    {
        //        _imagePath = value;
        //        OnPropertyChanged("ImagePath");
        //    }
        //}

        //public event PropertyChangedEventHandler PropertyChanged;

        //public void OnPropertyChanged([CallerMemberName]string prop = "")
        //{
        //    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        //}

        public static Session[] GetSessions()
        {
            return DB_Communication.SelectSessions();
        }
    }
}
