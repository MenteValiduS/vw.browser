﻿using LiveCharts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VW.Browser.Models
{
    class YearStatistic
    {
        public int year;
        public int[] playInMonths;

        public YearStatistic()
        {
            playInMonths = new int[12];
        }
    }
}
